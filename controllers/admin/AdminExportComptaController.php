<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/


<?php



class AdminExportComptaController extends ModuleAdminController {


    public function __construct(){

        parent::__construct();
        $this->context = Context::getContext();
      }

  function recupInfos()
  {
      //Toutes les informations que devra contenir le fichier sont récupérées ici et sont placees dans la variable $texte
      $cmd_verif_commandes_non_annulees = "SELECT `id_order` FROM `'._DB_PREFIX_.'orders`";
      $cmd_verif_commandes_non_annulees .= " WHERE `current_state` != 6";
      $verif_commandes_non_annulees = Db::getInstance()->ExecuteS($cmd_verif_commandes_non_annulees);
      $ligne = "";
      $texte = "";
      //on va récupérer les informations de toutes les commandes qui n'ont pas été annulées
      while ($row = $verif_commandes_non_annulees->fetch()) {
          $cmd_date_add = "SELECT `date_add` FROM `'._DB_PREFIX_.'orders`";
          $cmd_date_add .= " WHERE `id_order` = `'.$row[id_order].'`";
          $date_add = Db::getInstance()->getValue($cmd_date_add);
          $date_add = str_replace('-', '', $date_add);
          $date_add_split = str_split($date_add, 2);
          $mois = $date_add_split[2];
          $mois_actuel = date('m');
          //le script ne recupere que les enregistrements du mois en cours (remplacer par le mois dernier ?)
          if ($mois == $mois_actuel) {
              $ligne ++;
              $texte .= $ligne . ",";
              $journal = 'VE';
              $texte .= $journal . ",";
              $cmd_verif_paypal = "SELECT `product_id` FROM `'._DB_PREFIX_.'order_detail`";
              $cmd_verif_paypal .= " WHERE `product_id` = 7591";
              $verif_paypal = Db::getInstance()->getValue($cmd_verif_paypal);
              $cmd_id_produit = "SELECT `product_id` FROM `'._DB_PREFIX_.'order_detail`";
              $cmd_id_produit .= " WHERE `id_order` = `'.$row[id_order].'`";
              $id_produit = Db::getInstance()->getValue($cmd_id_produit);
              $cmd_verif_huiles = "SELECT `id_category` FROM `'._DB_PREFIX_.'product_shop`";
              $cmd_verif_huiles .= " WHERE `id_category` = 1092 and `id_product` = `'.$id_produit.'`";
              $verif_huiles = Db::getInstance()->getValue($cmd_verif_huiles);

              if (($verif_paypal->rowCount()) > 0) {
                  $paypal = true;
              }

              if (($verif_huiles->rowCount()) > 0) {
                  $huiles = 1;
              }

              if ($huiles and !$paypal) {
                  $compte = 445712;
              } elseif ($paypal) {
                  $compte = 467;
              } else {
                  $compte = 445711;
              }

              $texte .= $compte . ",";


              $sens = 'D';

              $texte .= $sens . ",";

              $cmd_piece = "SELECT `reference` FROM `'._DB_PREFIX_.'orders`";
              $cmd_piece .= "WHERE `id_order` = '.$row[id_order].'";

              $piece = Db::getInstance()->getValue($cmd_piece);

              $texte .= $piece . ",";

              $cmd_montant = "SELECT `total_paid_tax_excl` FROM `'._DB_PREFIX_.'orders`";
              $cmd_montant .= " WHERE `id_order` = '.$row[id_order].'";

              $montant = Db::getInstance()->getValue($cmd_montant);

              $texte .= $montant . ",";

              $cmd_id_devise = "SELECT `id_currency` FROM `'._DB_PREFIX_.'orders`";
              $cmd_id_devise = " WHERE `id_order` = '.$row[id_order].'";

              $id_devise = Db::getInstance()->getValue($cmd_id_devise);

              $cmd_devise = "SELECT `iso_code` FROM `'._DB_PREFIX_.'currency`";
              $cmd_devise .= " WHERE `id_currency` = '.$id_devise.' and  `id_order` = '.$row[id_order].'";

              $devise = Db::getInstance()->getValue($cmd_devise);

              $texte .= $devise . "\n";
          }
      }

      return $texte;
  }

  function creationFichier($texte)
  {
      recupInfos();
      file_put_contents('compta.txt', $texte);
  }

  $smarty = new smarty;
  if (Tools::isSubmit('btn_export')) {
      if (creationFichier()) {
          ?>
          <div class="block_content">
              <a href="compta.txt"> Télécharger le fichier de comptabilité </a>
          </div>
          <?php
      }
  }
}
